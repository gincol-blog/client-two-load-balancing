package es.gincol.ms.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MonitorController {

	private static final Logger log = LoggerFactory.getLogger(MonitorController.class);

	@GetMapping(value = "/readiness")
	public @ResponseBody String readiness() throws Exception {
		log.info("Acceso al servicio 'readiness'");
		return new StringBuilder().append("OK").toString();
	}

	@GetMapping(value = "/liveness")
	public @ResponseBody String liveness() throws Exception {
		log.info("Acceso al servicio 'liveness'");
		return new StringBuilder().append("OK").toString();
	}
	
}