package es.gincol.ms.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class MainController {

	private static Logger log = LoggerFactory.getLogger(MainController.class);

	@Value("${url.client-one.saludo}")
	private String urlSaludo;

//	@LoadBalanced
//	@Bean
//	RestTemplate restTemplate() {
//		return new RestTemplate();
//	}

	@Autowired
	RestTemplate restTemplate;

	@RequestMapping("/hola")
	public String hi(@RequestParam(value = "nombre", defaultValue = "Rigoberto") String nombre) {
		log.info("parametro recibido: " + nombre + ", accedermos a:  " + urlSaludo);
		String saludo = this.restTemplate.getForObject(urlSaludo, String.class);
		return String.format("%s %s!", saludo, nombre);
	}

}
