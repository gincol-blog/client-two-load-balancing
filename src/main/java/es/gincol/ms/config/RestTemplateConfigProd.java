package es.gincol.ms.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;

@Profile("prod")
@Configuration
public class RestTemplateConfigProd {

	@Bean
	public RestTemplate rest() {
		return new RestTemplate();
	}
}
